package sx.mxs.core.Listeners;

import com.sxtanna.recipe.Recipes;
import sx.mxs.core.Utils.CustomItems;

public final class CraftingListener {

    public CraftingListener() {
        Recipes.shaped(CustomItems.KNOWLEDGE_SCROLL)
                .put('M', CustomItems.MINE_DUST)
                .put('S', CustomItems.SKY_DUST)
                .define('M', 'M', ' ',
                        'S', 'S', ' ',
                        ' ', ' ', ' ').register();
    }

}