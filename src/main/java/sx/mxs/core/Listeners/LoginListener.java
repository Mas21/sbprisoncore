package sx.mxs.core.Listeners;

import net.md_5.bungee.api.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerKickEvent;
import org.bukkit.event.player.PlayerQuitEvent;

import java.util.HashSet;
import java.util.Set;


public class LoginListener implements Listener {

    public final Set<String> staff = new HashSet<>();
    public final Set<String> donors = new HashSet<>();

    @EventHandler(priority = EventPriority.HIGHEST)
    public void onJoin(PlayerJoinEvent j) {
        Player player = j.getPlayer();
        if (player.hasPermission("list.staff")) staff.add(player.getDisplayName());
        if (player.hasPermission("list.donor")) donors.add(player.getDisplayName());
    }

    @EventHandler(priority = EventPriority.HIGHEST)
    public void onLeave(PlayerQuitEvent leave) {
        Player player = leave.getPlayer();
        if (player.hasPermission("list.staff")) staff.remove(player.getDisplayName());
        if (player.hasPermission("list.donor")) donors.remove(player.getDisplayName());
    }

    @EventHandler(priority = EventPriority.HIGHEST)
    public void onKick(PlayerKickEvent leave) {
        Player player = leave.getPlayer();
        if (player.hasPermission("list.staff")) staff.remove(player.getDisplayName());
        if (player.hasPermission("list.donor")) donors.remove(player.getDisplayName());
    }

    @EventHandler(priority = EventPriority.HIGHEST, ignoreCancelled = true)
    public void onMOTD(PlayerJoinEvent e) {
        Player player = e.getPlayer();
        player.sendMessage("");
        player.sendMessage("");
        player.sendMessage("");
        player.sendMessage("");
        player.sendMessage("");
        player.sendMessage("");
        player.sendMessage("");
        player.sendMessage("");
        player.sendMessage("");
        player.sendMessage("");
        player.sendMessage("");
        player.sendMessage("");
        player.sendMessage("");
        player.sendMessage("");
        player.sendMessage("");
        player.sendMessage("");
        player.sendMessage("");
        player.sendMessage("");
        player.sendMessage("");
        player.sendMessage("");
        player.sendMessage("");
        player.sendMessage("");
        player.sendMessage("");
        player.sendMessage("");
        player.sendMessage("");
        player.sendMessage("");
        player.sendMessage("");
        player.sendMessage("");
        player.sendMessage("");
        player.sendMessage("");
        player.sendMessage("");
        player.sendMessage("");
        player.sendMessage("");
        player.sendMessage("");
        player.sendMessage("");
        player.sendMessage("");
        player.sendMessage("");
        player.sendMessage("");
        player.sendMessage("");
        player.sendMessage("");
        player.sendMessage("");
        player.sendMessage("");
        player.sendMessage("");
        player.sendMessage("");
        player.sendMessage("");
        player.sendMessage("");
        player.sendMessage("");
        player.sendMessage("");
        player.sendMessage(ChatColor.AQUA + "" + ChatColor.STRIKETHROUGH + "-------------------------" + ChatColor.DARK_GRAY + "" + ChatColor.STRIKETHROUGH + "---------------------------");
        player.sendMessage("");
        player.sendMessage(ChatColor.GRAY + "                    Welcome to " + ChatColor.AQUA + "" + ChatColor.BOLD + "" + "Skyblock " + ChatColor.DARK_GRAY + "" + ChatColor.BOLD + "Prison");
        player.sendMessage("                        " + ChatColor.GRAY + "" + ChatColor.ITALIC + "A prison like no other.");
        player.sendMessage("");
        player.sendMessage("");
        player.sendMessage("                 " + ChatColor.GREEN + "" + ChatColor.BOLD + "STORE " + ChatColor.WHITE + "" + ChatColor.UNDERLINE + "shop.skyprison.com");
        player.sendMessage("                 " + ChatColor.LIGHT_PURPLE + "" + ChatColor.BOLD + "DISCORD " + ChatColor.WHITE + "" + ChatColor.UNDERLINE + "skyprison.com/discord/");
        player.sendMessage("                 " + ChatColor.RED + "" + ChatColor.BOLD + "FORUMS " + ChatColor.WHITE + "" + ChatColor.UNDERLINE + "skyprison.com/forums/");
        player.sendMessage("");
        player.sendMessage(ChatColor.DARK_GRAY + "" + ChatColor.STRIKETHROUGH + "-------------------------" + ChatColor.AQUA + "" + ChatColor.STRIKETHROUGH + "---------------------------");
    }

}
