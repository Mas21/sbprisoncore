package sx.mxs.core.Utils;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;

import static sx.mxs.core.SBPrisonCore.perms;
import static sx.mxs.core.SBPrisonCore.tabFormats;

public class TabList implements Listener {

    @EventHandler(priority = EventPriority.HIGHEST)
    public void onJoin(PlayerJoinEvent e) {
        Player player = e.getPlayer();
        String tabPrefix = tabFormats.get(perms.getPrimaryGroup(player));
        if (tabPrefix != null) {
            player.setPlayerListName(tabPrefix);
        }
    }
}
