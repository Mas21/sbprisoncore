package sx.mxs.core.Utils;

import me.clip.placeholderapi.PlaceholderAPI;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.player.PlayerJoinEvent;

public class Scoreboard {
    @EventHandler(priority = EventPriority.HIGHEST)
    public void scoreboard(PlayerJoinEvent e) {
        String scoreboardTimePlayed;
        String scoreboardTimePlayedPAPI;
        Player player = e.getPlayer();
        scoreboardTimePlayed = "%statistic_time_played%";
        scoreboardTimePlayedPAPI = PlaceholderAPI.setPlaceholders(player, scoreboardTimePlayed);

    }
}

