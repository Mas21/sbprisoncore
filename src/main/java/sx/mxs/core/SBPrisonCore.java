package sx.mxs.core;

import com.sxtanna.recipe.Recipes;
import net.milkbowl.vault.permission.Permission;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.RegisteredServiceProvider;
import org.bukkit.plugin.java.JavaPlugin;
import sx.mxs.core.Commands.CmdDust;
import sx.mxs.core.Commands.CmdList;
import sx.mxs.core.Commands.CmdStaff;
import sx.mxs.core.Commands.CmdStop;
import sx.mxs.core.Commands.Essentials.CmdTeleport;
import sx.mxs.core.Listeners.*;
import sx.mxs.core.Utils.TabList;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

public final class SBPrisonCore extends JavaPlugin {

    public static Permission perms = null;
    public static Plugin plugin;

    public static Map<String, String> tabFormats = new HashMap<>();

    @Override
    public void onEnable() {
        final FileConfiguration config = this.getConfig();
        if (!new File(this.getDataFolder(), "config.yml").exists()) {
            config.options().copyDefaults(true);
        }
        saveDefaultConfig();
        //ConfigurationSection tabFormatsConfig = getConfig().getConfigurationSection("TabListFormats");
        //tabFormatsConfig.getKeys(false).forEach(key -> tabFormats.put(key, ChatColor.translateAlternateColorCodes('&', config.getString(key))));
        plugin = this;
        Recipes.enable(this);
        new CraftingListener();
        //setupPermission();
        this.listeners();
        this.commands();
    }

    public void listeners() {
        PluginManager pm = getServer().getPluginManager();
        pm.registerEvents(new RightClickListener(), this);
        pm.registerEvents(new BreakListener(), this);
        pm.registerEvents(new PlaceListener(), this);
        pm.registerEvents(new LoginListener(), this);
        pm.registerEvents(new TabList(), this);
        pm.registerEvents(new CmdStop(), this);
    }

    public void commands() {
        getCommand("list").setExecutor(new CmdList());
        getCommand("dust").setExecutor(new CmdDust());
        getCommand("staff").setExecutor(new CmdStaff());
        getCommand("teleport").setExecutor(new CmdTeleport());
    }

    private boolean setupPermission() {
        RegisteredServiceProvider<Permission> rsp = getServer().getServicesManager().getRegistration(Permission.class);
        perms = rsp.getProvider();
        return perms != null;
    }
}


