package sx.mxs.core.Commands.Essentials;

import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.Listener;

public class CmdTeleport implements Listener, CommandExecutor {

    public boolean onCommand(CommandSender sender, Command command, String s, String[] args) {
        Player player = (Player) sender;
        if (player.hasPermission("mas.teleport")) {
            if (args.length == 0) {
                player.sendMessage(ChatColor.RED + "You must specify a player.");
            } else if (args.length == 1) {
                if (player.getServer().getPlayer(args[0]) != null) {
                    Player targetPlayer = player.getServer().getPlayer(args[0]);
                    Location location = targetPlayer.getLocation();
                    player.teleport(location);
                    player.sendMessage(ChatColor.GREEN + "Tp'd you to " + targetPlayer.getName());
                } else {
                    player.sendMessage(ChatColor.RED + "Player not Online!");
                }
            }
        }
        return true;
    }

}