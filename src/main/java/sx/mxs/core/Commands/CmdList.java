package sx.mxs.core.Commands;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import sx.mxs.core.Listeners.LoginListener;

/**
 * Created by Mason Herbel on 6/9/2017.
 */
public class CmdList extends LoginListener implements CommandExecutor {
    @Override
    public boolean onCommand(CommandSender sender, Command command, String s, String[] strings) {
        if (sender instanceof Player) {
            Player player = (Player) sender;
            player.sendMessage(ChatColor.GRAY + "Online: " + Bukkit.getOnlinePlayers().size());
            player.sendMessage(ChatColor.GREEN + "Donors: " + donors.toString().replace("[", "").replace("]", "").trim());
            player.sendMessage(" ");
            player.sendMessage(ChatColor.DARK_RED + "Staff: " + staff.toString().replace("[", "").replace("]", "").trim());
        }
        return true;
    }
}
