package sx.mxs.core.Commands;

import org.bukkit.Bukkit;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerCommandPreprocessEvent;

import java.util.concurrent.TimeUnit;

public class CmdStop implements Listener {

    @EventHandler(priority = EventPriority.LOWEST)
    public void onCommandPreProcess(PlayerCommandPreprocessEvent event) throws InterruptedException {
        if(event.getMessage().toLowerCase().startsWith("/stop")) {
            if (event.getPlayer().hasPermission("mas.stop")) {
                event.setCancelled(true);
                Bukkit.getOnlinePlayers().forEach(p -> p.sendTitle("§cServer restart in...", ""));
                TimeUnit.SECONDS.sleep(1);
                Bukkit.getOnlinePlayers().forEach(p -> p.sendTitle("§c5", "§f..seconds until reboot"));
                TimeUnit.SECONDS.sleep(1);
                Bukkit.getOnlinePlayers().forEach(p -> p.sendTitle("§c4", "§f..seconds until reboot"));
                TimeUnit.SECONDS.sleep(1);
                Bukkit.getOnlinePlayers().forEach(p -> p.sendTitle("§c3", "§f..seconds until reboot"));
                TimeUnit.SECONDS.sleep(1);
                Bukkit.getOnlinePlayers().forEach(p -> p.sendTitle("§c2", "§f..seconds until reboot"));
                TimeUnit.SECONDS.sleep(1);
                Bukkit.getOnlinePlayers().forEach(p -> p.sendTitle("§c1", "§f..seconds until reboot"));
                TimeUnit.SECONDS.sleep(1);
                Bukkit.getOnlinePlayers().forEach(p -> p.sendTitle("§cServer restarting!", "Be right back!"));
                Bukkit.getServer().shutdown();
            }
        }
    }

}
